package com.collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
class Employee
{
  int id;
  String name;
  double salary;
public Employee() {
	super();
	
}
public Employee(int id, String name, double salary) {
	super();
	this.id = id;
	this.name = name;
	this.salary = salary;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getSalary() {
	return salary;
}
public void setSalary(double salary) {
	this.salary = salary;
}
@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
}

  
  
}
public class ArrayListExample {
	
	public static void main(String[] args) {
		List<Employee> names = new LinkedList<>();
		System.out.println(names.isEmpty());;
		names.add(new Employee(101,"Amruta",300000));
		names.add(new Employee(102,"Amar",300000));
		names.add(new Employee(103,"Preety",300000));
		System.out.println(names.size());;
		System.out.println(names);
		
		//names.remove(1);
		System.out.println(names.size());;
		System.out.println(names);
		//names.add(0, "Amodini");
		System.out.println(names);
		
	}

}
