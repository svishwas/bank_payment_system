package com.collection;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		
		HashSet<Integer> number = new HashSet<>();
		number.add(101);
		number.add(102);
		number.add(103);
		number.add(104);
		number.add(105);
		number.add(106);
		number.add(107);
		number.add(209);
		number.add(205);
		number.add(206);
		number.add(207);
		number.add(208);
		number.add(209);
		number.add(301);
		number.add(302);
		number.add(304);
		number.add(305);
		number.add(306);

		
		System.out.println(number);
	}
}
